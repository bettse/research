# http -v POST https://stage.knocki.com/users "X-Device-ID: 0" < test.json
{
    "data": [
        {
            "attributes": {
                "confirmationId": "e2da052e-6e80-4018-bf57-96b9a718c8d2",
                "disabled": false,
                "email": "remu@simpleemail.info",
                "failedLoginCount": 0,
                "id": "8d4280a3-0253-4476-946d-4ac2311d4fa7",
                "ipaddress": [
                    "174.127.219.28",
                    "70.132.31.77"
                ],
                "role": "user"
            },
            "type": "users"
        }
    ],
    "included": [
        {
            "attributes": {
                "id": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI4ZDQyODBhMy0wMjUzLTQ0NzYtOTQ2ZC00YWMyMzExZDRmYTciLCJlbWFpbCI6InJlbXVAc2ltcGxlZW1haWwuaW5mbyIsInB3ZCI6IjcyMUQxNTQyRDgiLCJ0ZiI6ZmFsc2UsImFnZW50IjoiIiwiaWF0IjoxNTU3MzY2OTUwfQ.CKVGis4I1nNiftMZKlZvhZ3LLxz1WG-HvoUwzDtMWN4",
                "type": "auth"
            },
            "type": "tokens"
        }
    ]
}


# http -v PUT https://stage.knocki.com/tokens/e2da052e-6e80-4018-bf57-96b9a718c8d2 "X-Device-ID: 0" < confirm.json
{
    "data": [
        {
            "attributes": {
                "code": "7599",
                "confirmedAt": 1557367467485,
                "createdAt": 1557366949563,
                "debug": false,
                "expiresAt": "2020-05-08T01:55:49.563Z",
                "id": "e2da052e-6e80-4018-bf57-96b9a718c8d2",
                "retry": 0,
                "type": "confirmation",
                "updatedAt": 1557367467485,
                "user": "8d4280a3-0253-4476-946d-4ac2311d4fa7"
            },
            "type": "tokens"
        }
    ]
}
