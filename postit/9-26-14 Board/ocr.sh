

for file in *.jpg; do convert $file -resize 400% -type Grayscale ${file/jpg/tif}; done
for file in *.tif; do tesseract -l eng $file ${file/.tif/}; done
