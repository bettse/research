# General

AS is Alaska
AS-002 is Batman vs Superman movie (AS-* are movies)

saved http://airbornemedia.gogoinflight.com/asp/api/config


http://airborne.gogoinflight.com/static/js/as-scripts.js?
```
/**
* @Airline:   AS
* @Name:       as-scripts.js
* @Location: /static/js/aa-scripts.js
* @URI:          <<<< change
* @Last Updated: 12/7/15 by jwilburn
*/
```

# Resolves DNS
bettse@Erics-MacBook-Pro ~ $ host ericbetts.org
ericbetts.org has address 192.30.252.153
ericbetts.org has address 192.30.252.154
bettse@Erics-MacBook-Pro ~ $ host 192.30.252.153
153.252.30.192.in-addr.arpa domain name pointer pages.github.com.

# http requests redirected to portal

http http://trackladd.ericbetts.org
HTTP/1.1 302 Temporarily Moved
Location: http://airborne.gogoinflight.com/abp/page/abpDefault.do?REP=127.0.0.1&AUTH=127.0.0.1&CLI=172.19.131.119&PORT=54273&RPORT=54272&acpu_redirect=true

# iOS simulator

loaded bing over https in ios simulator.  Loaded bing using httpie over https, slowly, once

# 2 Up hosts besides me
bettse@Erics-MacBook-Pro ~ $ nmap -sP 172.19.131.1-254

Starting Nmap 7.12 ( https://nmap.org ) at 2017-01-26 18:21 EST
Nmap scan report for 172.19.131.2
Nmap scan report for 172.19.131.119
Nmap scan report for 172.19.131.250

## Host A: 172.19.131.2 (http://airborne.gogoinflight.com)

### portscan

Starting Nmap 7.12 ( https://nmap.org ) at 2017-01-26 18:24 EST
Nmap scan report for 172.19.131.2
Host is up (0.0062s latency).
Not shown: 998 filtered ports
PORT     STATE SERVICE
80/tcp   open  http
3128/tcp open  squid-http

### HTTP

Via: 1.1 172.19.134.2 (squid/3.5.10)
X-Powered-By: pil-vm-app-52

HTTP/1.1 302 Temporarily Moved
Location: http://airborne.gogoinflight.com/abp/page/abpDefault.do?REP=127.0.0.1&AUTH=127.0.0.1&CLI=172.19.131.119&PORT=54273&RPORT=54272&acpu_redirect=true


### Squid

Generated Thu, 26 Jan 2017 23:25:57 GMT by 172.19.134.2 (squid/3.5.10)

## Host B: 172.19.131.250 (airbornemedia.gogoinflight.com)

### a few ports

bettse@Erics-MacBook-Pro ~ $ nmap 172.19.131.250
Starting Nmap 7.12 ( https://nmap.org ) at 2017-01-26 18:23 EST
Nmap scan report for 172.19.131.250
Host is up (0.0055s latency).
Not shown: 993 closed ports
PORT     STATE    SERVICE
22/tcp   filtered ssh
80/tcp   open     http
1000/tcp open     cadlock
1443/tcp open     ies-lm
8009/tcp open     ajp13
9998/tcp open     distinct32
9999/tcp open     abyss

### 404 for `/` on 80, 1000

bettse@Erics-MacBook-Pro ~ $ http http://172.19.131.250
HTTP/1.1 404 Not Found
Content-Length: 0
Date: Thu, 26 Jan 2017 23:22:50 GMT
Server: Apache-Coyote/1.1

bettse@Erics-MacBook-Pro ~ $ http http://172.19.131.250:1000
HTTP/1.1 404 Not Found
Content-Length: 0
Date: Thu, 26 Jan 2017 23:22:58 GMT
Server: Apache-Coyote/1.1

### 200 for `/app`

### ios app workaround

on simulator: http://airbornemedia.gogoinflight.com/app/#compatibility/check/AS-022
on chrome: http://airbornemedia.gogoinflight.com/app/#compatibility/check/AS-002


* Watch: `data-automation="actionHaveApp"`
* Download iOS app: `data-automation="actionNeedeApp"`
  http://airborne.gogoinflight.com/gbp/bypassmode.do?execution=e2s1
  http://airborne.gogoinflight.com/gbp/captcha.jpg?
  captcha form POSTS to `/gbp/bypassmode.do?execution=e2s1`    
  302 to http://airborne.gogoinflight.com/gbp/connecting.do?flowExecutionKey=e2s1&flowExecutionUrl=%2Fgbp%2Fbypassmode.do%3Fexecution%3De2s1
  302 to http://airborne.gogoinflight.com/gbp/connecting.do?execution=e3s1
  302 to http://airborne.gogoinflight.com/abp/page/aaaActivate.do?gbpsessionid=DE2A25059A3A274E3BF4C54B42DCC782.22
  302 to http://airborne.gogoinflight.com/gbp/gobrowse.do;jsessionid=DE2A25059A3A274E3BF4C54B42DCC782.22
  302 to http://airbornemedia.gogoinflight.com/app/index.html#captcha/success/AS-002
  FREE WIFI


`"click .userPrompt .needIOSApp":"redirectToCaptcha"`
window.location.href = u.config.get("iOSCaptchaURL") + "?" + n
"iOSCaptchaURL": "http://airborne.gogoinflight.com/gbp/appDownLoad.do",

http://airborne.gogoinflight.com/gbp/appDownLoad.do?boxArtUrl=http%3A%2F%2Fcs.video.gogoinflight.com%2Fmedia%2FASCZFF0322%2FASCZFF0322_bm.jpeg&mediaTitle=Batman%20V%20Superman%3A%20Dawn%20of%20Justice&mediaId=AS-002
302 to http://airborne.gogoinflight.com/gbp/bypassmode.do
302 to http://airborne.gogoinflight.com/gbp/bypassmode.do?execution=e6s1


curl 'http://airborne.gogoinflight.com/gbp/captcha.jpg?' -H 'Pragma: no-cache' -H 'DNT: 1' -H 'Accept-Encoding: gzip, deflate, sdch' -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1' -H 'Accept: image/webp,image/*,*/*;q=0.8' -H 'Referer: http://airborne.gogoinflight.com/gbp/bypassmode.do?execution=e4s1' -H 'Cookie: languageCookie=en_US; currencyCookie=USD; bypass_used=N461AS; ggvtrailer=false; fxCookie=1D49F809A8898E67F50B312919BA5BA8.22; testCookieName=testCookieName; JSESSIONID=1D49F809A8898E67F50B312919BA5BA8.22; _sdsat_Access Technology=ATG4; _sdsat_Flight Region Code=DOM; video=true; GG_VE_STATUS=true; counterSDI=9; s_cc=true; s_fid=5277ED43EC2FE62C-16EEDEF7E04F2431; s_sq=%5B%5BB%5D%5D' -H 'Connection: keep-alive' -H 'Cache-Control: no-cache' --compressed


#### Question (text) captcha

http://airborne.gogoinflight.com/gbp/bypassmode.do?execution=e6s1&_eventId=vcCaptcha
