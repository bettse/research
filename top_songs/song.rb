require 'scrapify'

module Song
  class Alternative
    include Scrapify::Base
    html "http://www.popvortex.com/music/charts/top-alternative-songs.php"

    attribute :position, css: 'p.chart-position'
    attribute :title, css: 'cite.title a'
    attribute :artist, css: 'em.artist span'

    key :position

    def inspect
      "#<Alternative ##{position}: #{artist} - #{title}>"
    end

  end
end

