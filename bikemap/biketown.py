#!/usr/bin/env python

import time, sys, os
from mitmproxy.script import concurrent
from mitmproxy.models import decoded
import json
from geojson import GeometryCollection, Point, Feature, FeatureCollection
import geojson

def response(context, flow):
  if not flow.match("~d app.socialbicycles.com"):
    return
  with decoded(flow.response):
    if flow.match("~u bikes.json"):
      features = []
      response = json.loads(flow.response.content)
      for bike in response['items']:
        point = bike['current_position']
        f = Feature(geometry=point, id=bike['id'], properties=bike)
        features.append(f)

      fc = FeatureCollection(features)
      dump = geojson.dumps(fc, sort_keys=True)
      f = open('ui/bikes.json', 'w')
      f.write(dump)

    if flow.match("~u hubs.json"):
      features = []
      response = json.loads(flow.response.content)
      for hub in response['items']:
        polygon = hub['polygon']
        f = Feature(geometry=polygon, id=hub['id'], properties=hub)
        features.append(f)

      fc = FeatureCollection(features)
      dump = geojson.dumps(fc, sort_keys=True)
      f = open('ui/hubs.json', 'w')
      f.write(dump)

# vim: set tabstop=2 shiftwidth=2 expandtab : #
