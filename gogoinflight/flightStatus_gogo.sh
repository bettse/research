#!/usr/bin/env bash
#
# <bitbar.title>Flight Status</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>Eric Betts</bitbar.author>
# <bitbar.author.github>bettse</bitbar.author.github>
# <bitbar.dependencies>shell,jq</bitbar.dependencies>
#

export PATH="/usr/local/opt/coreutils/libexec/gnubin/:/usr/local/bin:/usr/bin:$PATH"
EMOJI="✈"

STATUS=$(curl -s http://airborne.gogoinflight.com/abp/ws/absServices/statusTray)
if [[ -z "${STATUS// }" ]]; then
  echo "Grounded"
  exit
fi

EXP_ARRIVAL_STRING=$(echo $STATUS | jq -r '.Response .flightInfo .expectedArrival')
FROM=$(echo $STATUS | jq -r '.Response .flightInfo .departureAirportCodeIata')
TO=$(echo $STATUS | jq -r '.Response .flightInfo .destinationAirportCodeIata')
FLIGHT=$(echo $STATUS | jq -r '.Response .flightInfo .flightNumberInfo')

EXP_ARRIVAL=$(date --date=$EXP_ARRIVAL_STRING "+%s")
NOW=$(date "+%s")

SEC=$(($EXP_ARRIVAL - $NOW))
HOURS=$(($SEC / 3600))
SEC=$(expr $SEC % 3600)
MINUTES=$(expr $SEC / 60)

printf "${EMOJI} %d:%02d\n" ${HOURS} ${MINUTES}
echo "---"
echo "${FROM} ${FLIGHT} ${EMOJI} ${HOURS}Hr ${MINUTES}Min Remaining ${TO}"

