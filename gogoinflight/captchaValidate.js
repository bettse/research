/**
	* Captcha Validate JS
	* Last Updated: 10/14/15 11:50am
	* last change: added logic for
*/

// $(document).ready(function(){

	var captchaFunction = function captchaFunction(){

	$("body").addClass('captcha');

    $('#utilityNav').insertBefore($('#myLib_btn'));
    $('#myLib_btn').hide();

    $("#paymentWrap > div > p").prepend($('#boxArt').clone());

	$('#lvHelp_ftr').click(function(){
			var dest = '/gbp/echat.do';
			window.open(dest,'chatWindow','width=624,height=600,resizable=0,scrollbars=yes,location=no,status=no');
			return false;
	});

	// pop-up for Live Help:
	$("a.liveHelp, a#exceedChatLink").click(function(){
		var dest = 'https://custhelp.gogoinflight.com/app/home/flightNumber/'+flightNo+'/flightOrigin/'+flightOrigin+'/flightDestination/'+flightDest+'/tailNumber/'+tailNumber+'/macAddress/'+ma;
		window.open(dest,'chatWindow','width=624,height=600,resizable=0,scrollbars=yes,location=no,status=no');
		return false;
	});
	// define dest variable
	var dest = 'https://custhelp.gogoinflight.com/app/home/flightNumber/'+flightNo+'/flightOrigin/'+flightOrigin+'/flightDestination/'+flightDest+'/tailNumber/'+tailNumber+'/macAddress/'+ma;

//if backend error
	if($('#errorMsg').html().length > 1){
		$('#captchaEntered').addClass('error');
		$('#captchaInput .error').append($('#errorMsg'));
	}


	//trim function
	if(typeof String.prototype.trim !== 'function') {
		String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
		}
	}

	//check if boxart is not available, then remove
	var boxArt = $('#boxArt').html();

	if(boxArt.trim() == ''){
		$('#boxArt').remove();
	}

	//logic for exceeded number of downloads of app loophole
	var exceedHtml = "<div id=\"exceed\"><h2>Download limit exceeded</h2><div class=\"exceedText\"><p class=\"exceedBold\">You've exceeded the maximum number of download attempts.</p><p class=\"exceedPlease\">Please <a id=\"exceedChatLink\" href=\"#\" target=\"_blank\">chat with Customer Care</a> to get the Gogo Video Player.</p><div class=\"exceedButton\"><a id=\"exceedOk\" href=\"splash.do\">Ok</a></div></div></div>";
	if(videoByPass==="false"){
		$('#main').remove();
		$('#contentMain').prepend($(exceedHtml));

		$("#exceedChatLink").attr("href", dest);
	}

//submit
	$('#captchaForm').submit(function(e){

		var textVal = $('#captchaEntered').val();

		if($('.VICaptchaWrap').length){

			var isnum = /^\d+$/.test(textVal);

			if (textVal == '') {

				$('#captchaInput .error').html(messages.videoCaptcha.failVI);
				$('#captchaEntered').addClass('error');
				e.preventDefault();
				return false;
			}
			else if (!isnum) {
				$('#captchaInput .error').html(messages.videoCaptcha.numbersOnly);
				$('#captchaEntered').addClass('error');
				e.preventDefault();
				return false;
			}
		}
		else {
			if (textVal == '' || textVal.length != 5) {

				$('#captchaInput .error').html(messages.videoCaptcha.fail);
				$('#captchaEntered').addClass('error');
				e.preventDefault();
				return false;
			}
		}
		$('#captchaInput .error').html('');
		$('#captchaEntered').removeClass('error');
		return true;
	});

}// END captchaFunction;


var isAcaptchaPage = false;
var nonCaptchaPages = [
	'pickAPass'
];


$(document).ready(function(){
	// if(document.body.className.indexOf('pickAPass') < 0){
	// if(document.body.className.indexOf('pickAPass') < 0){
	// 	captchaFunction();
	// }

	for (i = 0; i < nonCaptchaPages.length; i++) {
		if(document.body.className.indexOf(nonCaptchaPages)[i] < 0){
			isAcaptchaPage = true;
		}
	}

	if(isAcaptchaPage == true){
		captchaFunction();
	}

});
