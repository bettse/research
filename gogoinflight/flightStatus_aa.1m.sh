#!/usr/bin/env bash
#
# <bitbar.title>Flight Status</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>Eric Betts</bitbar.author>
# <bitbar.author.github>bettse</bitbar.author.github>
# <bitbar.dependencies>shell,jq</bitbar.dependencies>
#

export PATH="/usr/local/opt/coreutils/libexec/gnubin/:/usr/local/bin:/usr/bin:$PATH"
EMOJI="✈"

STATUS=$(curl -s https://aa.viasat.com/device)
if [[ -z "${STATUS// }" ]]; then
  echo "Grounded"
  exit
fi

MINUTES=$(echo $STATUS | jq -r '.flightMinutesRemaining')
FROM=$(echo $STATUS | jq -r '.flightOrigin')
TO=$(echo $STATUS | jq -r '.flightDestination')
FLIGHT=$(echo $STATUS | jq -r '.flightNumber')

HOURS=$(($MINUTES / 60 ))
MINUTES=$(expr $MINUTES % 60)


printf "${EMOJI} %d:%02d\n" ${HOURS} ${MINUTES}
echo "---"
echo "${FROM} ${FLIGHT} ${EMOJI} ${HOURS}Hr ${MINUTES}Min Remaining ${TO}"

