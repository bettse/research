const fs = require('fs');

const fs_0 = fs.readFileSync('./fs_0_out.bin');

let index = 0x200;

const chunks = [];

do {
  const file_id = fs_0.readUInt16LE(index);
  index += 2;
  const chunk_id = fs_0.readUInt16LE(index);
  index += 2;
  const chunk_size = fs_0[index];
  if (chunk_size !== 0xfc) {
    //console.log('problem at', {index, file_id, chunk_id, chunk_size}, 'skipping');
    index += 0xfc;
    continue;
  }
  index++;
  const chunk = fs_0.slice(index, index + chunk_size - 1);

  //console.log({index, file_id, chunk_id, chunk_size, chunk: chunk.toString()})
  chunks.push({
    file_id, chunk_id, chunk,
  })

  index += chunk_size - 1;

  if (index % 0x1000 === 0) {
    index += 0x100;
  }

} while(index < fs_0.length);

console.log(chunks.map(chunk => chunk.chunk).map(x => x.toString()).join(''))
