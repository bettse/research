# HD minicam

Camera in a USB power adapter

Maybe called 'esn'

- One app is 'esndo'
  - Functions inside prefixed `ESNP2P_`
- Folder called 'esntd'

## URLs

- http://jurgis.me/2019/07/24/hacking-chinese-mini-ip-camera/

## Firmware

### /etc_ro/passwd

password: "" (empty string)

### /etc_ro/passwd~

password: "helpme"
