# HAAS Alert

Responder-to-Vehicle (R2V) 
Responder to Responder (R2R)

## Power



## USB

### lsusb

`Bus 001 Device 006: ID 2b04:c00a`


### dmesg

```
[1647366.358221] usb 1-1.2: new full-speed USB device number 6 using xhci_hcd
[1647366.498111] usb 1-1.2: New USB device found, idVendor=2b04, idProduct=c00a, bcdDevice= 2.50
[1647366.498121] usb 1-1.2: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[1647366.498127] usb 1-1.2: Product: Electron
[1647366.498133] usb 1-1.2: Manufacturer: Particle
[1647366.498139] usb 1-1.2: SerialNumber: 310057000650483553353520
[1647366.547906] cdc_acm 1-1.2:1.0: ttyACM0: USB ACM device
[1647366.551054] usbcore: registered new interface driver cdc_acm
[1647366.551065] cdc_acm: USB Abstract Control Model driver for USB modems and ISDN adapters
```


### pio device monitor --raw -b 230400 -p /dev/cu.usbmodem8342101




#### 5v

```
0000072231 [app] WARN: Very low battery, VBAT: 4.9232 < 11.9V
    72.607 AT read  +   14 "\r\n+CIEV: 3,0\r\n"
    72.608 AT read  +   14 "\r\n+CIEV: 7,0\r\n"
    72.610 AT read  +   14 "\r\n+CIEV: 9,0\r\n"
CIEV matched: 9,0
    72.709 AT send      10 "AT+CPIN?\r\n"
    72.716 AT read ERR  32 "\r\n+CME ERROR: SIM not inserted\r\n"
    73.718 AT send      10 "AT+CPIN?\r\n"
    73.725 AT read ERR  32 "\r\n+CME ERROR: SIM not inserted\r\n"
0000074232 [app] WARN: Very low battery, VBAT: 4.9323 < 11.9V
    74.727 AT send      10 "AT+CPIN?\r\n"
    74.734 AT read ERR  32 "\r\n+CME ERROR: SIM not inserted\r\n"

[ Modem::powerOff ] = = = = = = = = = = = = = =
    74.735 AT send       4 "AT\r\n"
    74.738 AT read OK    6 "\r\nOK\r\n"
    74.738 AT send      12 "AT+CPWROFF\r\n"
0000076233 [app] WARN: Very low battery, VBAT: 4.9413 < 11.9V
0000078233 [app] WARN: Very low battery, VBAT: 4.9511 < 11.9V
```

```
0000005196 [app.ubloxgps] INFO: UBX response timeout
0000005196 [app.ubloxgps] INFO: UBX baudrate is 115200
0000005196 [app.ubloxgps] INFO: enable PUBX-POSITION
0000008198 [app.ubloxgps] INFO: UBX response timeout
0000008198 [app.ubloxgps] INFO: enable PUBX-SVSTATUS
0000011199 [app.ubloxgps] INFO: UBX response timeout
0000011199 [app.ubloxgps] INFO: enable PUBX-TIME
0000014200 [app.ubloxgps] INFO: UBX response timeout
0000014200 [app.ubloxgps] INFO: enablePUBX error: 0x07
0000017201 [app.ubloxgps] INFO: UBX response timeout
0000020202 [app.ubloxgps] INFO: UBX response timeout
0000023203 [app.ubloxgps] INFO: UBX response timeout
0000026204 [app.ubloxgps] INFO: UBX response timeout
0000029205 [app.ubloxgps] INFO: UBX response timeout
0000032206 [app.ubloxgps] INFO: UBX response timeout
0000035207 [app.ubloxgps] INFO: UBX response timeout
0000038208 [app.ubloxgps] INFO: UBX response timeout
0000038208 [app.ubloxgps] INFO: disableNMEA error: 0xFF
0000041209 [app.ubloxgps] INFO: UBX response timeout
0000044210 [app.ubloxgps] INFO: UBX response timeout
0000044210 [app.ubloxgps] INFO: enable GPS
0000044210 [app.ubloxgps] INFO: enable QZSS
0000044210 [app.ubloxgps] INFO: enable BeiDou
0000044211 [app.ubloxgps] INFO: enable GLONASS
0000047212 [app.ubloxgps] INFO: UBX response timeout
0000047212 [app.ubloxgps] INFO: set to power management mode 0
0000050213 [app.ubloxgps] INFO: UBX response timeout
0000050213 [app.ubloxgps] INFO: set dynamic platform model to 4
0000053214 [app.ubloxgps] INFO: UBX response timeout
0000053215 [app] INFO: APP FW Version 18
0000053216 [app] INFO: heartbeatIntervalSec=3600
0000053218 [app] INFO: Application reset reason:8, Reset count:1150
0000053222 [NV Config] INFO: LOAD:[HEADER     ] CALC: 0xBD63A4C1 CRC: 0xBD63A4C1 OFF: 4096   SIZE: 64  
0000053227 [NV Config] INFO: LOAD:[HEADER     ] CALC: 0xBD63A4C1 CRC: 0xBD63A4C1 OFF: 8192   SIZE: 64  
0000053231 [NV Config] INFO: LOAD:[HEADER     ] CALC: 0xBD63A4C1 CRC: 0xBD63A4C1 OFF: 0      SIZE: 64  
0000053235 [app] INFO: Status LED State: OFF_GPS_FIXING
0000053235 [app] INFO: Light bar: Active
0000053239 [app] INFO: {"AX":0.0000,"AY":0.0000,"AZ":0.0000,"AT":true}
0000053240 [app] INFO: {"ACC-X":0.0000,"ACC-Y":0.0000,"ACC-Z":0.0000,"MAGNITUDE":0.0000}
0000053242 [app] INFO: {"ODO":0,"CAL":-1,"GSIG":0,"SIG":1,"QUAL":0,"PGD":false,"SUP":53,"CUP":0,"MEM":42952,"BLK":42184,"RSC":1150,"RSR":8}
0000053243 [app] INFO: Status LED State: RESPONDING
0000053244 [app] INFO: Wed Dec  1 00:10:21 1999: RESPONDING: 0.00 >= 20.00
0000053245 [app] INFO: App State: RESPONDING
0000053244 [app] INFO: Status LED State: RESPONDING
[ ElectronSerialPipe::begin ] pipeTx=0 pipeRx=0

[ Modem::powerOn ] = = = = = = = = = = = = = =
```





#### 12v:

Same except no more lower battery complaints



#### Flashing Lights Master +12v

```
0000012308 [app] INFO: ESF_STATUS[sensor7]  :r:1 u:0 t:18 ts:1 cs:0 fq:9 bf:0
0000013836 [app] INFO: Light bar: Active
0000013836 [app] INFO: Status LED State: RESPONDING
0000013837 [app] INFO: Wed Dec  1 00:01:58 1999: RESPONDING: 0.00 >= 20.00
0000013837 [app] INFO: App State: RESPONDING
    18.594 AT read OK    6 "\r\nOK\r\n"
```


#### https://docs.particle.io/hardware/expansion/about-serial/#baud-rate-bits-parity-and-stop-bits-gen-2-

>  Each of these commands only requires that you type the command letter (case-sensitive):

    i - Prints the device ID (24 character hexadecimal string)
    f - Firmware update (using ymodem)
    x - Exit listening mode
    s - Print system_module_info
    v - Device OS version
    L - Safe listen mode (does not run user code concurrently with listening mode)
    w - Configure Wi-Fi
    m - Print MAC Address for the Wi-Fi adapter

Listening mode is the default when you plug in a Photon the first time. You can also get into listening mode by holding down SETUP for about 3 seconds until the status LED blinks blue.

The commands other then the last two Wi-Fi related commands are also available on the Electron.

----

i

```
Device ID: 310057000650483553353520
IMEI: 358887093664827
ICCID: 
```

s

```
[ Modem::init ] = = = = = = = = = = = = = = =
   205.727 AT send       9 "AT+CGSN\r\n"
   205.727 AT read  +   14 "\r\n+CIEV: 3,0\r\n"
   205.731 AT read  +   14 "\r\n+CIEV: 7,0\r\n"
   205.732 AT read  +   14 "\r\n+CIEV: 9,0\r\n"
CIEV matched: 9,0
   205.733 AT read  +   14 "\r\n+CIEV: 9,1\r\n"
CIEV matched: 9,1
   205.734 AT read  +   14 "\r\n+CIEV: 2,1\r\n"
   205.743 AT read UNK  19 "\r\n358887093664827\r\n"
   205.744 AT read OK    6 "\r\nOK\r\n"
SIM not inserted

{"p":10,"imei":"358887093664827","iccid":"","m":[{"s":16384,"l":"m","vc":30,"vv":30,"f":"b","n":"0","v":400,"d":[]},{"s":131072,"l":"m","vc":30,"vv":30,"f":"s","n":"1","v":1405,"d":[{"f":"s","n":"3","v":1405,"_":""}]},{"s":131072,"l":"m","vc":30,"vv":30,"f":"s","n":"2","v":1405,"d":[{"f":"s","n":"1","v":1405,"_":""},{"f":"b","n":"0","v":400,"_":""}]},{"s":131072,"l":"m","vc":30,"vv":30,"f":"s","n":"3","v":1405,"d":[{"f":"s","n":"2","v":110,"_":""}]},{"s":131072,"l":"m","vc":30,"vv":30,"u":"83C507E4C02968DA537BCD379DC45834DC89F33ED50E0260D65885A9F5022475","f":"u","n":"1","v":6,"d":[{"f":"s","n":"2","v":1405,"_":""}]}]}
```

v: `system firmware version: 1.4.3`

f: `Waiting for the binary file to be sent ... (press 'a' to abort)`

#### $ particle serial inspect

`screen /dev/ttyACM0`
`x`
ctrl+a \

`stty -F /dev/ttyACM0 ispeed 28800`


```
Platform: 10 - Electron
Modules
  Bootloader module #0 - version 400, main location, 16384 bytes max size
    Integrity: PASS
    Address Range: PASS
    Platform: PASS
    Dependencies: PASS
  System module #1 - version 1405, main location, 131072 bytes max size
    Integrity: PASS
    Address Range: PASS
    Platform: PASS
    Dependencies: PASS
      System module #3 - version 1405
  System module #2 - version 1405, main location, 131072 bytes max size
    Integrity: PASS
    Address Range: PASS
    Platform: PASS
    Dependencies: PASS
      System module #1 - version 1405
      Bootloader module #0 - version 400
  System module #3 - version 1405, main location, 131072 bytes max size
    Integrity: PASS
    Address Range: PASS
    Platform: PASS
    Dependencies: PASS
      System module #2 - version 110
  User module #1 - version 6, main location, 131072 bytes max size
    UUID: 83C507E4C02968DA537BCD379DC45834DC89F33ED50E0260D65885A9F5022475
    Integrity: PASS
    Address Range: PASS
    Platform: PASS
    Dependencies: PASS
      System module #2 - version 1405
```

#### $ particle serial identify

```
Your device id is 310057000650483553353520
Your IMEI is 358887093664827
Your system firmware version is 1.4.3
```
