var map = L.map('map');
var hash = new L.Hash(map);

L.tileLayer.provider('OpenStreetMap.Mapnik', {retina: true}).addTo(map);
//Add UI controls to toggle later
//L.tileLayer.provider('Watercolor').addTo(map);

var bikeOptions = {
  interval: 10 * 1000,
  style: function(feature) { return feature.properties; },
  pointToLayer: customStyle,

  onEachFeature: function (feature, layer) {
    layer.bindPopup(feature.properties.name);
    layer.on('mouseover', function (e) {
      this.openPopup();
    });
    layer.on('mouseout', function (e) {
      this.closePopup();
    });
  }
}

var hubOptions = {
  interval: 10 * 1000,
  onEachFeature: function (feature, layer) {
    layer.bindPopup(feature.properties.name + "\n" + feature.properties.description);
    layer.on('mouseover', function (e) {
      this.openPopup();
    });
    layer.on('mouseout', function (e) {
      this.closePopup();
    });
  }
};

var bikes = L.realtime({url: 'bikes.json', type: 'json'}, bikeOptions).addTo(map);
var hubs = L.realtime({url: 'hubs.json', type: 'json'}, hubOptions).addTo(map);

var loaded = false;
bikes.on('update', function(updateEvent) {
  if(!loaded && Object.keys(updateEvent.features).length > 0) {
    loaded = true;
    //Set the view after the first load
    map.fitBounds(bikes.getBounds(), {});
  }
});

//https://gist.github.com/tmcw/3861338
function simplestyle(f, latlon) {
    var sizes = {
      small: [20, 50],
      medium: [30, 70],
      large: [35, 90]
    };
    var fp = f.properties || {};
    var size = fp['marker-size'] || 'medium';
    var symbol = (fp['marker-symbol']) ? '-' + fp['marker-symbol'] : '';
    var color = fp['marker-color'] || '7e7e7e';
    color = color.replace('#', '');
    var url = 'http://a.tiles.mapbox.com/v3/marker/' +
          'pin-' +
          // Internet Explorer does not support the `size[0]` syntax.
          size.charAt(0) + symbol + '+' + color +
          ((window.devicePixelRatio === 2) ? '@2x' : '') +
          '.png';

    var icon = new L.icon({
        iconUrl: url,
        iconSize: sizes[size],
        iconAnchor: [sizes[size][0] / 2, sizes[size][1]/2],
        popupAnchor: [-3, -sizes[size][1]/2]
      });

    return new L.Marker(latlon, {
      icon: icon
    });
}

function customStyle(f, latlon) {
  var properties = f.properties;

  if(properties.state == 'available') {
    properties['marker-color'] = 'FFA500';
    if (properties.hub_id == null) {
      properties['marker-color'] = 'ff0000';
    }
  } else {
    properties['marker-color'] = 'ffffff';
    properties['name'] += "\n" + properties.state;
  }

  f.properties = properties;
  var marker = simplestyle(f, latlon);

  return marker;
}

