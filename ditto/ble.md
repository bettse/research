peripheral with ID 7fe21df95bc947b49eeb33c8123e6980 found
  Local Name        = ditto  
  Service Data      = 
  Service UUIDs     = 4ab97a4c63f04c7a85f0afb07ce65d41

services and characteristics:
000015301212efde1523785feabcd123
  000015321212efde1523785feabcd123
    properties  writeWithoutResponse
  000015311212efde1523785feabcd123
    properties  write, notify
4ab97a4c63f04c7a85f0afb07ce65d41
  4ab97a4d63f04c7a85f0afb07ce65d41
    properties  writeWithoutResponse, write
  4ab97a4e63f04c7a85f0afb07ce65d41
    properties  writeWithoutResponse, write
  4ab97a4f63f04c7a85f0afb07ce65d41
    properties  writeWithoutResponse, write
  4ab97a5063f04c7a85f0afb07ce65d41
    properties  writeWithoutResponse, write
  4ab97a5163f04c7a85f0afb07ce65d41
    properties  writeWithoutResponse, write
  4ab97a5263f04c7a85f0afb07ce65d41
    properties  writeWithoutResponse, write
  4ab97a5363f04c7a85f0afb07ce65d41
    properties  read, notify
    value       0104021607066300ad3b727e000000000000000000 | 'c-;r~'
180f (Battery Service)
  2a19 (Battery Level)
    properties  read, notify
    value       5e | '^'
180a (Device Information)
  2a29 (Manufacturer Name String)
    properties  read
    value       53696d706c654d617474657273 | 'SimpleMatters'
  2a24 (Model Number String)
    properties  read
    value       44313031 | 'D101'
  2a25 (Serial Number String)
    properties  read
    value       414335343538414233304645 | 'AC5458AB30FE'
  2a27 (Hardware Revision String)
    properties  read
    value       4631 | 'F1'

